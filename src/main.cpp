// MIT License
// https://github.com/gonzalocasas/arduino-uno-dragino-lorawan/blob/master/LICENSE
// Based on examples from https://github.com/matthijskooijman/arduino-lmic
// Copyright (c) 2015 Thomas Telkamp and Matthijs Kooijman

#include <Arduino.h>
#include "lmic.h"
#include <hal/hal.h>
#include <SPI.h>
#include <SSD1306.h>
#include <CayenneLPP.h>
#include <TinyGPS++.h>
#include <HardwareSerial.h>
//#include <Adafruit_GPS.h>

#define LEDPIN 23

#define OLED_I2C_ADDR 0x3C
#define OLED_RESET 16
#define OLED_SDA 21 //4
#define OLED_SCL 22 //15

unsigned int counter = 0;
char TTN_response[30];

SSD1306 display(OLED_I2C_ADDR, OLED_SDA, OLED_SCL);

// This EUI must be in little-endian format, so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes.
// This should also be in little endian format, see above.
//For TTN issued EUIs the last bytes should be 0xD5, 0xB3,
// 0x70.
//static const u1_t PROGMEM APPEUI[8]={ 0x50, 0xF9, 0x00, 0xD0, 0x7E, 0xD5, 0xB3, 0x70 };   // Chose LSB mode on the console and then copy it here.
static const u1_t PROGMEM APPEUI[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; // Chose LSB mode on the console and then copy it here.
//static const u1_t PROGMEM APPEUI[8]={ 0xCB, 0x8C, 0x28, 0xCB, 0x4B, 0xAF, 0x55, 0x00 };
void os_getArtEui(u1_t *buf) { memcpy_P(buf, APPEUI, 8); }

//static const u1_t PROGMEM DEVEUI[8]={ 0xAA, 0x09, 0x76, 0xF5, 0x6C, 0x93, 0xDA, 0x00 };   // LSB mode
static const u1_t PROGMEM DEVEUI[8] = {0x65, 0x57, 0x63, 0x44, 0x54, 0x36, 0x44, 0x21}; // LSB mode
//2144365444635765
void os_getDevEui(u1_t *buf) { memcpy_P(buf, DEVEUI, 8); }

// This key should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from ttnctl can be copied as-is.
// The key shown here is the semtech default key.
//static const u1_t PROGMEM APPKEY[16] = { 0xCD, 0xBF, 0xC4, 0x7E, 0x7A, 0xA5, 0x85, 0x8B, 0xAB, 0x84, 0x08, 0xC0, 0x45, 0xD1, 0xE2, 0xA4 }; // MSB mode
static const u1_t PROGMEM APPKEY[16] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; // MSB mode
void os_getDevKey(u1_t *buf) { memcpy_P(buf, APPKEY, 16); }

static osjob_t sendjob;

// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
const unsigned TX_INTERVAL = 30;

CayenneLPP lpp(51);

// GPS
float latitude;
float longitude;
float altitude;

TinyGPSPlus gps;
#define GPS_RX 34
#define GPS_TX 35
HardwareSerial GPSSerial(1);

// LMIC enhanced Pin mapping
const lmic_pinmap lmic_pins = {
    .nss = 18,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 14,
    .dio = {26, 33, 32}};

void get_coords()
{
    while (GPSSerial.available())
    {
        gps.encode(GPSSerial.read());
    }
    latitude = gps.location.lat();
    longitude = gps.location.lng();
    altitude = gps.altitude.meters();
    Serial.println(latitude);
    Serial.println(longitude);
    Serial.println(gps.location.age());
    Serial.println(gps.satellites.value());
}

void do_send(osjob_t *j)
{
    // Payload to send (uplink)
    //static uint8_t message[] = "Hello OTTA!";

    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND)
    {
        Serial.println(F("OP_TXRXPEND, not sending"));
    }
    else
    {
        // Prepare upstream data transmission at the next possible time.
        //get_coords();
        //if (gps.location.isValid())
        //{
        // Prepare upstream data transmission at the next possible time.
        lpp.reset();
        lpp.addTemperature(1, random(10, 30));
        lpp.addBarometricPressure(1, random(1000, 1050));
        lpp.addGPS(1, latitude, longitude, altitude);
        //LMIC_setTxData2(1, message, sizeof(message)-1, 0);
        LMIC_setTxData2(2, lpp.getBuffer(), lpp.getSize(), 0);
        //LMIC_setTxData2(1, data, 10, 0);
        //LMIC_setTxData2(1, latlong.bytes, 12, 0);
        //LMIC_setTxData2(1, latitude + longitude + altitude, sizeof(latitude) + sizeof(longitude) + sizeof(altitude), 0);
        Serial.println(F("Sending uplink packet..."));
        digitalWrite(LEDPIN, HIGH);
        delay(1000);
        display.clear();
        display.drawString(0, 0, "Sending uplink packet...");
        display.drawString(0, 50, String(++counter));
        display.display();
        //}
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

void onEvent(ev_t ev)
{
    Serial.print(os_getTime());
    Serial.print(": ");
    switch (ev)
    {
    case EV_TXCOMPLETE:
        Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
        display.clear();
        display.drawString(0, 0, "EV_TXCOMPLETE event!");

        if (LMIC.txrxFlags & TXRX_ACK)
        {
            Serial.println(F("Received ack"));
            display.drawString(0, 20, "Received ACK.");
        }

        if (LMIC.dataLen)
        {
            int i = 0;
            // data received in rx slot after tx
            Serial.print(F("Data Received: "));
            Serial.write(LMIC.frame + LMIC.dataBeg, LMIC.dataLen);
            Serial.println();
            Serial.println(LMIC.rssi);

            display.drawString(0, 9, "Received DATA.");
            for (i = 0; i < LMIC.dataLen; i++)
                TTN_response[i] = LMIC.frame[LMIC.dataBeg + i];
            TTN_response[i] = 0;
            display.drawString(0, 22, String(TTN_response));
            display.drawString(0, 32, String(LMIC.rssi));
            display.drawString(64, 32, String(LMIC.snr));
        }

        // Schedule next transmission
        os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
        digitalWrite(LEDPIN, LOW);
        display.drawString(0, 50, String(counter));
        display.display();
        break;
    case EV_JOINING:
        Serial.println(F("EV_JOINING: -> Joining..."));
        display.drawString(0, 16, "OTAA joining....");
        display.display();
        break;
    case EV_JOINED:
    {
        Serial.println(F("EV_JOINED"));
        display.clear();
        display.drawString(0, 0, "Joined!");
        display.display();
        // Disable link check validation (automatically enabled
        // during join, but not supported by TTN at this time).
        LMIC_setLinkCheckMode(0);
    }
    break;
    case EV_RXCOMPLETE:
        // data received in ping slot
        Serial.println(F("EV_RXCOMPLETE"));
        break;
    case EV_LINK_DEAD:
        Serial.println(F("EV_LINK_DEAD"));
        break;
    case EV_LINK_ALIVE:
        Serial.println(F("EV_LINK_ALIVE"));
        break;
    default:
        Serial.println(F("Unknown event"));
        break;
    }
}

void setup()
{
    Serial.begin(115200);
    delay(2500); // Give time to the serial monitor to pick up
    Serial.println(F("Starting..."));
    // hardware serial for  GPS
    Serial.println("(I) - init GPS");
    //GPSSerial.begin(9600, SERIAL_8N1, GPS_RX, GPS_TX);
    //GPSSerial.setTimeout(2);

    // Use the Blue pin to signal transmission.
    pinMode(LEDPIN, OUTPUT);

    //Set up and reset the OLED
    pinMode(OLED_RESET, OUTPUT);
    digitalWrite(OLED_RESET, LOW);
    delay(50);
    digitalWrite(OLED_RESET, HIGH);

    display.init();
    display.flipScreenVertically();
    display.setFont(ArialMT_Plain_10);

    display.setTextAlignment(TEXT_ALIGN_LEFT);

    display.drawString(0, 0, "Starting....");
    //display.drawXbm(0, 0, 128, 64, logot);
    display.display();
    //delay(5000);

    // LMIC init
    Serial.println("LMIC INIT");
    os_init();

    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();
    //LMIC_setClockError(MAX_CLOCK_ERROR * 1 / 100);
    // Set up the channels used by the Things Network, which corresponds
    // to the defaults of most gateways. Without this, only three base
    // channels from the LoRaWAN specification are used, which certainly
    // works, so it is good for debugging, but can overload those
    // frequencies, so be sure to configure the full frequency range of
    // your network here (unless your network autoconfigures them).
    // Setting up channels should happen after LMIC_setSession, as that
    // configures the minimal channel set.

    LMIC_setupChannel(0, 923200000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(1, 923400000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI); // g-band
    LMIC_setupChannel(2, 923600000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(3, 923800000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(4, 924000000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(5, 924200000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(6, 924400000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    LMIC_setupChannel(7, 924600000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);  // g-band
    //LMIC_setupChannel(8, 924800000, DR_RANGE_MAP(DR_FSK, DR_FSK), BAND_MILLI);   // g2-band

    /*
    LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
    LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK,  DR_FSK),  BAND_MILLI);      // g2-band
    */
    // TTN defines an additional channel at 869.525Mhz using SF9 for class B
    // devices' ping slots. LMIC does not have an easy way to define set this
    // frequency and support for class B is spotty and untested, so this
    // frequency is not configured here.

    // Disable link check validation
    LMIC_setLinkCheckMode(0);

    // TTN uses SF9 for its RX2 window.
    //LMIC.dn2Dr = DR_SF9;

    // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
    //LMIC_setDrTxpow(DR_SF11,14);
    LMIC_setDrTxpow(DR_SF7, 14);

    // Start job
    do_send(&sendjob); // Will fire up also the join
    //LMIC_startJoining();
    //os_runloop_once();
}

void loop()
{
    os_runloop_once();
    //do_send(&sendjob);
    //delay(10000);
}
